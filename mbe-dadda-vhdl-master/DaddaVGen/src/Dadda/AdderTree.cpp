// Copyright (C) 2019 João Pedro de Omena Simas
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "AdderTree.hh"

std::map<int, std::vector<std::shared_ptr<Signal>>> AdderTree::setColumns(size_t N_bits){
  std::map<int, std::vector<std::shared_ptr<Signal>>> columns;
  // i represents the counter for each column
  for(size_t i = 0; i <= N_bits; i++) {
    std::vector<std::shared_ptr<Signal>> column;
    // j represents the counter in each column
    auto it = column.begin();
    for(size_t j = 0; j <= i/2; j++) {
      it = column.insert(it, std::make_shared<Signal>(Signal::signal_type::P, j, i - 2*j));
      it++;
    }

    if(i%2 == 0) {
      it = column.insert(it, std::make_shared<Signal>(Signal::signal_type::neg, i/2, NA));
      it++;
    }
      
    columns[i] = column;
  }
  for(size_t i = N_bits + 1; i <= 2*N_bits/*-1*/; i++) {
    std::vector<std::shared_ptr<Signal>> column;
    // j represents the counter in each column

    auto it = column.begin();
    if((i - N_bits - 1)%2 == 0) {
      it = column.insert(it, std::make_shared<Signal>(Signal::signal_type::nNeg, (i - N_bits - 1)/2, NA));
      it++;
    } else {
      it = column.insert(it, std::make_shared<Signal>(Signal::signal_type::one, NA, NA));
      it++;
    }
    
    for(size_t j = (i - N_bits + 1)/2; j <= N_bits/2; j++) {
      it = column.insert(it, std::make_shared<Signal>(Signal::signal_type::P, j, i - 2*j));
      it++;
    }
    columns[i] = column;
  }

  columns[N_bits + 1].insert(columns[N_bits + 1].begin(), std::make_shared<Signal>(Signal::signal_type::one, NA, NA));

  return columns;
}

int AdderTree::getTreeSize(){
  size_t maxTreeSize = PP_tree.find(0)->second.size();
  for(size_t i = 1; i < PP_tree.size();  i ++){
    if (maxTreeSize < PP_tree.find(i)->second.size())
      maxTreeSize = PP_tree.find(i)->second.size();
  }
  return maxTreeSize;
}

void AdderTree::addHalfAdder(int col){
  auto S = std::make_shared<Signal>(Signal::signal_type::S, n_adder, NA);
  auto Cout = std::make_shared<Signal>(Signal::signal_type::Cout, n_adder, NA);
  auto it = PP_tree.find(col);
  if(it == PP_tree.end())
    throw std::runtime_error("AdderTree::addHalfAdder: invalid column number.");
  auto half_adder = std::make_shared<HA>(it->second.at(0), it->second.at(1), S, Cout);
  n_adder++;

  it->second.erase(it->second.begin());
  it->second.erase(it->second.begin());
  it->second.push_back(S);

  it = PP_tree.find(col + 1);
  if(it == PP_tree.end()) {
    PP_tree[col + 1] = std::vector<std::shared_ptr<Signal>>();
    PP_tree[col + 1].push_back(Cout);
  } else
    it->second.push_back(Cout);
		
  Adders.push_back(half_adder);
}

void AdderTree::addFullAdder(int col){
  auto S = std::make_shared<Signal>(Signal::signal_type::S, n_adder, NA);
  auto Cout = std::make_shared<Signal>(Signal::signal_type::Cout, n_adder, NA);
  auto it = PP_tree.find(col);
  if(it == PP_tree.end())
    throw std::runtime_error("AdderTree::addFullAdder: invalid column number.");
  auto full_adder = std::make_shared<FA>(it->second.at(0), it->second.at(1), it->second.at(2), S, Cout);
  n_adder++;
  
  it->second.erase(it->second.begin());
  it->second.erase(it->second.begin());
  it->second.erase(it->second.begin());
  it->second.push_back(S);

  it = PP_tree.find(col + 1);
  if(it == PP_tree.end()) {
    PP_tree[col + 1] = std::vector<std::shared_ptr<Signal>>();
    PP_tree[col + 1].push_back(Cout);
  } else
    it->second.push_back(Cout);
		
  Adders.push_back(full_adder);
}

void AdderTree::daddaReduction(size_t minReduction){
  for(size_t col = 0; col < PP_tree.size(); col ++){
    auto column = PP_tree.find(col)->second;
    int n_loop = 0;
    while(column.size() > minReduction){
      if(column.size() > minReduction + 1)
	      addFullAdder(col);
      else
	      addHalfAdder(col);
      column = PP_tree.find(col)->second;
      n_loop++;
      
      if(n_loop > 1000000)
	throw std::runtime_error("AdderTree::daddaReduction: too many loops. column.size() = " + std::to_string(column.size()) + ". minReduction = " + std::to_string(minReduction));
    }
  }
};

void AdderTree::setDaddaAdders(size_t N_bits){
  constexpr double ReductionRatio = 1.5;
  int height = 2;
  PP_tree = setColumns(N_bits);  // all partial product columns
  int maxSize = getTreeSize(); // Maximum column size

  while(ReductionRatio * height < maxSize)
    height = floor(height * ReductionRatio);
  
  while(maxSize > 2) {
    daddaReduction(height);
    // update maxSize and Dadda height
    maxSize = getTreeSize();
    height = ceil(height / ReductionRatio);
  }
    
  // column 0 only has one partial product
  // column 1 will be left with S[0], the output of a half adder

}

void AdderTree::genVHDL(size_t N_bits){
  std::ofstream file;
  file.open("vhdl/Mult_Dadda" + std::to_string(N_bits) + ".vhd", std::ofstream::out);
  
  file << "library ieee;";
  file << "\nuse ieee.std_logic_1164.all;";
  file << "\nuse ieee.numeric_std.all;";
	    
  file << "\n\nentity Mult_Dadda" + std::to_string(N_bits) + " is";
  file << "\ngeneric(N : natural := " + std::to_string(N_bits) + ");";

  file << "\nport(x : in std_logic_vector(N - 1 downto 0);";
  file << "\ny: in std_logic_vector(N - 1 downto 0);";
  file << "\nz : out std_logic_vector(2*N - 1 downto 0));";

  file << "\nend entity Mult_Dadda" + std::to_string(N_bits) + ";";

  file << "\narchitecture arch of Mult_Dadda" + std::to_string(N_bits) + " is";

  file << "\ncomponent full_adder is\nport(\na : in std_logic;\nb : in std_logic;\ncin : in  std_logic;\ns : out std_logic;\ncout : out std_logic);\nend component;";

  file << "\ncomponent half_adder is\nport(\na : in std_logic;\nb : in std_logic;\ns : out std_logic;\ncout : out std_logic);\nend component;\n";

  file << "\ncomponent mbe_ppg is\ngeneric(n : natural := 32);\nport(three_digits : in std_logic_vector(2 downto 0);--sign bit representation\na : in std_logic_vector(N-1 downto 0);\npp : out std_logic_vector(N downto 0);\nneg : out std_logic);\nend component;";

  file << "\n\ntype p_t is array (N/2 downto 0) of std_logic_vector(N downto 0);";
  file << "\nsignal p : p_t;";
  file << "\nsignal neg, n_neg : std_logic_vector(N/2 downto 0);";
  file << "\nsubtype s_t is std_logic_vector(" + std::to_string(n_adder - 1) + " downto 0);";
  file << "\nsignal S : s_t;";
  file << "\nsubtype cout_t is std_logic_vector(" + std::to_string(n_adder - 1) + " downto 0);";
  file << "\nsignal Cout : cout_t;";
  file << "\nsignal ppg0_three_digits, ppglast_three_digits : std_logic_vector(2 downto 0);";
  file << "\nsignal sum, carry, result : std_logic_vector(2*N+1 downto 0);";
  file << "\nbegin";

  file << "\nppg0_three_digits <= x(1 downto 0)&'0';";
  file << "\nppg0: mbe_ppg generic map(N) port map(ppg0_three_digits, y, P(0), neg(0));";
  file << "\nF0: for i in 1 to n/2-1 generate";
  file << "\nppgi: mbe_ppg generic map(N) port map(x(2*i + 1 downto 2*i - 1), y, P(i), neg(i));\nend generate;";

  file << "\nppglast_three_digits(n - 2*(n/2) downto 0) <= x(n - 1 downto 2*(n/2) -1);";
  file << "ppglast_three_digits(2 downto n - 2*(n/2) + 1) <= (others => '0');";
  file << "\nppglast: mbe_ppg generic map(N) port map(ppglast_three_digits, y, P(n/2), neg(n/2));";
 
  int HAcnt = 1;
  int FAcnt = 1;
  for(auto& adder: Adders) {
    if (dynamic_cast<HA*>(adder.get())){
      file << "\nHA" + std::to_string(HAcnt) + " : half_adder port map" + adder->toString() + ";";
      HAcnt++;
    }
    else if (dynamic_cast<FA*>(adder.get())){
      file << "\nFA" + std::to_string(FAcnt) + " : full_adder port map" + adder->toString() + ";";
      FAcnt++;
    }
  }

  file << "\nsum <= \"0\" & S(" + std::to_string(n_adder-1) + " downto " + std::to_string(n_adder - (N_bits * 2) + 1) + ") & \"0\" & neg(0);";
  file << "\ncarry <= Cout(" + std::to_string(n_adder-1) + " downto " + std::to_string(n_adder - (N_bits * 2) + 1) + ") & neg(1) & P(0)(1) & P(0)(0);";

  file << "\nresult <= std_logic_vector(unsigned(sum) + unsigned(carry));";

  file << "\nF1: for i in 0 to neg'length-1 generate\nn_neg(i) <= not neg(i);\nend generate;";

  //for(size_t i = 0; i <= 2*N_bits - 1; i++){
  //  auto it = PP_tree.find(i);
  //  if(it != PP_tree.end()){
  //    file << "\nz(" << i << ") <= " << it->second.at(0)->toString() << ";";
  //  }
  //}
  file << "\nz <= result(N*2-1 downto 0);";

  file << "\n\nend architecture;";
  
  file.close();

  for(size_t i = 0; i < PP_tree.size(); i++) {
    auto it = PP_tree.find(i);
    if(it != PP_tree.end()) {
      std::cout << "col: " << i << " size:" << it->second.size() << std::endl;
    }
  }

  
}
