\chapter{Implementation of a Booth multiplier}
\label{chap2}

In this second part of the lab, it is requested to design a Modified Booth Encoder (MBE). Since the radix was not specified, 
the radix-4 approach was chosen. This choice comes from the fact that it gives a good trade-off between speed and 
complexity. It is also specified that the adder plane must rely on a Dadda-tree and the sign extension bits in the Dadda-tree 
must be simplified in order to reduce the overall number of half adders and full adders.

In order to fulfil the requirements different sources were consulted and studied. In particular, a approach to automated generation of HDL 
Dadda multipliers (de Castro et al., 2017) was chosen as a reference. The basic ideas of the paper were carefully studied and integrated with the theoretical lectures attended 
regarding this topic. Thus, the main key points dealing with the Dadda implementation and algorithm are described in the following paragraphs. 

The Dadda reduction algorithm shown in \ref{alg:1} consists of applying half-adders and full-adders in each column to reduce the number of operators in a layer under the desired Dadda height. 
Considering that the final partial product matrix have a height of 2 (i.e. one or two elements at each column of the partial 
product matrix), the first Dadda height is 2, and each subsequent Dadda height is determined by the previous height multiplied by 1.5 
and rounded by a floor function. Following this schema, the starting value of desired height is achieved by getting the first Dadda 
height that is smaller than the longest column of the partial product matrix. The following algorithm shows the implementation of the Dadda reduction schema, 
where for each column Col\textsubscript{c} whose height is larger than the desired Dadda height, a full-adder grouping is applied if the difference 
between the heights is greater than 1, and a half-adder grouping is applied otherwise. This reduction process is applied until the 
height of the larger column in the partial product matrix is equal to two. 

\begin{algorithm}[ht]
  \SetAlgoNoLine
  \DontPrintSemicolon
  $weight\gets$ length of \textit{PP\_matrix}'s longest column\;
  $desired_height\gets$ max($Dadda\_height<weight$)\;
  $M\gets$ number of columns in \textit{PP\_matrix}\;
  \While{$weight>2$}{
    \For{$c\in\{2,\ldots,M-1\}$}{
      $length_c\gets$ length of \textit{Col\textsubscript{c}}\;
      \uIf{$length_c>desired\_height+1$}{
        Apply full-adder grouping at \textit{Col\textsubscript{c}}\;
      } \ElseIf{$length_c>desired\_height$}{
        Apply half-adder grouping at \textit{Col\textsubscript{c}}\;
      }
    }
    $weight\gets$ length of \textit{PP\_matrix}'s longest column\;
    $desired\_height\gets\ceil*{\frac{desired\_height}{1.5}}$\;
  }
  \caption{Dadda multiplier reduction schema}
  \label{alg:1}
\end{algorithm}

Furthermore, algorithm \ref{alg:2} shows the Verilog code generation schema, which takes as input the list of all HA and FA 
objects created during the reduction processes.

\begin{algorithm}[ht]
  \SetAlgoNoLine
  \DontPrintSemicolon
  \For{$i\in\{0,\ldots,N-1\}$}{
    \For{$j\in\{0,\ldots,N-1\}$}{
      write "assign P[i][j] $=$ x[i] \& y[j];"\;
    }
  }
  $A\gets$ Amount of adders in \textit{Adders} list\;
  write "wire[A-1:0] S;"\;
  write "wire[A-1:0] Cout;"\;
  $cnt\gets1$\;
  \For{$adder\in Adders$}{
    \uIf{\textit{adder} is \textit{HA}}{
      write "Half\_Adder HA\textsubscript{cnt}(\textit{list\_of\_signals});"\;
      $cnt/getscnt+1$\;
    } \ElseIf{\textit{adder} is \textit{FA}}{
      write "Full\_Adder FA\textsubscript{cnt}(\textit{list\_of\_signals});"\;
      $cnt/getscnt+1$\;
    }
  }
  $n\gets2N-1$\;
  \For{$signal\in Outputs$}{
    write "assign z[n] $=$ signal;"\;
    $n\gets n-1$\;
  }
  write "endmodule"\;
  \caption{Verilog code generation schema}
  \label{alg:2}
\end{algorithm}

Afterwards, the sign extension procedure in the Booth multiplier has also to be taken into account since the sign extension bits in 
the Dadda-tree must be simplified to reduce the overall number of half adders and full adders. Figure \ref{fig:sign_ext} summarises the procedure.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{chapters/figures/sign_ext.jpg}
  \caption{Sign extension for the Booth multiplier}
  \label{fig:sign_ext}
\end{figure}

In order to proceed with the VHDL implementation of the unsigned multiplier, it was decided to use a script, which according to the described 
algorithm in figure, it automatically generates the VHDL code for the computation of all the partial products and the reduced dadda tree. 
The choice of using a script was made because of the fact that the probability of error is much more reduced in this way. The script, combined 
with the VHDL code describing the Booth partial product generation completes the VHDL description of the first two steps of the design of the unsigned multiplier.

The used script is written using the C++ language. All the columns are put in a map of vectors where each vector contains a specific column of a specific 
layer. The first part of the code is the following:

\lstinputlisting[language=C++, breaklines=true]{./chapters/files/setColumns.txt}

As it can be observed, the dots of the highest layer are mapped in C++. In this part, the sign extension is also taken into account according to 
the algorithm described in figure \ref{fig:sign_ext}. The following part of the code places full adders and half adders for each column in the considered layer 
according to algorithm \ref{alg:1}.

\lstinputlisting[language=C++, breaklines=true]{./chapters/files/dadda_reduction.txt}

Then a function calls the Dadda reduction algorithm to reduce all the layers of a given tree:

\lstinputlisting[language=C++, breaklines=true]{./chapters/files/set_dadda_adders.txt}

The last part of the script generates the VHDL code, which combined with the VHDL implementation of the Booth’s algorithm, puts together the 
partial product matrix generation and the tree reduction according to the Dadda approach. The third and final step for completing the VHDL 
implementation consists in adding toghether the last two generated partial products of the lowest layer. As suggested, this last sum was implemented 
behaviourally to take advantage of the optimizations possible in the synthesis step. 

The produced VHDL description code of the unsigned multiplier was simulated with ModelSim and the correctness of the results was verified. As 
shown in the following figure, the results are as expected. Afterwards, the unsigned multiplier was used as a component in the Floating Point 
Multiplier and was tested again with ModelSim.