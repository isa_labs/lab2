\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Digital arithmetic assignement}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Digital arithmetic and logic synthesis}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Fine-grain Pipelining and optimization}{2}{section.1.2}%
\contentsline {chapter}{\numberline {2}Implementation of a Booth multiplier}{3}{chapter.2}%
\contentsline {chapter}{\numberline {3}Results, Comparisons and Conclusions}{8}{chapter.3}%
\contentsline {section}{\numberline {3.1}Comparisons}{8}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Multiplier Implementation}{8}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Impact of the optimization commands}{9}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Optimization of the Booth multiplier implementation}{9}{subsection.3.1.3}%
