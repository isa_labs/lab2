source /software/scripts/init_msim6.2g

rm -r work
vlib work

#compile
vcom -93 -work ./work ../mbe-dadda-vhdl-master/DaddaVGen/src/Dadda/vhdl/*.vhd

#start simulation
vsim -novopt work.tb32 -do ./mult_sim.tcl



