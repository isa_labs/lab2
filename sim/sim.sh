source /software/scripts/init_msim6.2g

rm -r work
vlib work

#compile

vcom -93 -work ./work ../mbe-dadda-vhdl-master/DaddaVGen/src/Dadda/vhdl/*.vhd
vcom -93 -work ./work ../src/*.vhd

vcom -93 -work ./work ../fpuvhdl/adder/fpalign_struct.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpinvert_fpinvert.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fplzc_fplzc.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpselcomplement_fpselcomplement.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpswap_fpswap.vhd
vcom -93 -work ./work ../fpuvhdl/common/fpnormalize_fpnormalize.vhd
vcom -93 -work ./work ../fpuvhdl/common/fpround_fpround.vhd
vcom -93 -work ./work ../fpuvhdl/common/packfp_packfp.vhd
vcom -93 -work ./work ../fpuvhdl/common/unpackfp_unpackfp.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpadd_normalize_struct.vhd

vcom -93 -work ./work ../fpuvhdl/adder/fpadd_single_cycle.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpadd_stage1_struct.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpadd_stage2_struct.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpadd_stage3_struct.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpadd_stage4_struct.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpadd_stage5_struct.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpadd_stage6_struct.vhd
vcom -93 -work ./work ../fpuvhdl/adder/fpadd_pipeline.vhd

vcom -93 -work ./work ../fpuvhdl/multiplier/fpmul_single_cycle.vhd
vcom -93 -work ./work ../fpuvhdl/multiplier/fpmul_stage1_struct.vhd
vcom -93 -work ./work ../fpuvhdl/multiplier/fpmul_stage2_struct.vhd
vcom -93 -work ./work ../fpuvhdl/multiplier/fpmul_stage3_struct.vhd
vcom -93 -work ./work ../fpuvhdl/multiplier/fpmul_stage4_struct.vhd
vcom -93 -work ./work ../fpuvhdl/multiplier/fpmul_pipeline.vhd

vcom -93 -work ./work ../tb/*.vhd

vlog -work ./work ../tb/tb_fp.v

#start simulation
vsim -c -novopt work.tb_fp -do ./simulation.tcl
#vsim work.tb_fp


