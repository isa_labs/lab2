library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;

library std;
use std.textio.all;

entity data_maker is  
  port (
	RST_n : in std_logic;
    CLK : in  std_logic;
    A   : out std_logic_vector(31 downto 0);
    B   : out std_logic_vector(31 downto 0);
    VOUT : out std_logic;
    END_SIM : out std_logic);
end data_maker;

architecture beh of data_maker is

  constant tco : time := 1 ns;

  signal sEndSim : std_logic;
  signal END_SIM_i : std_logic_vector(0 to 10);  

begin  -- beh

  process (CLK)
    file fp_in : text open READ_MODE is "fp_samples.hex";
    variable line_in : line;
    variable x : std_logic_vector(31 downto 0);
  begin  -- process
    if RST_n = '0' then
      A <= (others => 'X');
      A <= (others => 'X');
      VOUT <= '0';
      sEndSim <= '0';
    elsif CLK'event and CLK = '1' then  -- rising clock edge
      if not endfile(fp_in) then
        readline(fp_in, line_in);
        hread(line_in,x);-- for reading hex values
        A <= x;
        B <= x; 
        VOUT <= '1';
        sEndSim <= '0';
      else
        VOUT <= '0';
        sEndSim <= '1';       
      end if;
    end if;
  end process;

  process (CLK, RST_n)
  begin  -- process
    if RST_n = '0' then                 -- asynchronous reset (active low)
      END_SIM_i <= (others => '0') after tco;
    elsif CLK'event and CLK = '1' then  -- rising clock edge
      END_SIM_i(0) <= sEndSim after tco;
      END_SIM_i(1 to 10) <= END_SIM_i(0 to 9) after tco;
    end if;
  end process;

  END_SIM <= END_SIM_i(10);  
 
end beh;
