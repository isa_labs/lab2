library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;

library std;
use std.textio.all;

entity data_sink is
  port (
	RST_n : in std_logic;
    CLK   : in std_logic;
	VIN : in std_logic;
    Z_IN   : in std_logic_vector(31 downto 0));
end data_sink;

architecture beh of data_sink is

component flipflopD is   
	port (D :	in std_logic;
				clk :	in std_logic;
				rst :	in std_logic; -- reset active low
				en : in std_logic;
				Q :	out	std_logic);
end component;

signal VIN1, VIN2, VIN3, VIN4, VIN5, VIN6 : std_logic;

begin  -- beh

	ff0 : flipflopD port map (VIN, CLK, RST_n, '1', VIN1);
	ff2 : flipflopD port map (VIN1, CLK, RST_n, '1', VIN2);
	ff3 : flipflopD port map (VIN2, CLK, RST_n, '1', VIN3);
	ff4 : flipflopD port map (VIN3, CLK, RST_n, '1', VIN4);	
	ff5 : flipflopD port map (VIN4, CLK, RST_n, '1', VIN5);
	ff6 : flipflopD port map (VIN5, CLK, RST_n, '1', VIN6);	

  process (CLK)
    file res_fp : text open WRITE_MODE is "./results_mult.hex";
    variable line_out : line;    
  begin  -- process
   if RST_n = '0' then 
		null;
   elsif CLK'event and CLK = '1' then  -- rising clock edge
	if VIN6 = '1'then
      hwrite(line_out,Z_IN);
      writeline(res_fp, line_out);
	end if;
   end if;
  end process;


end beh;
