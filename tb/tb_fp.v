//`timescale 1ns

module tb_fp();

   wire CLK_i;
   wire [31:0] A_i;
   wire [31:0] B_i;
   wire [31:0] Z_i;
   wire VOUT_i;
   wire END_SIM_i;
   wire RST_n_i;
  
  

   clk_gen CG( .END_SIM(END_SIM_i),
               .CLK(CLK_i),
               .RST_n(RST_n_i));
	      

   data_maker SM( .RST_n(RST_n_i), 
			         .CLK(CLK_i),
	               .A(A_i),
		            .B(B_i),
		            .VOUT(VOUT_i),
                  .END_SIM(END_SIM_i));

   FPmul UUT(.CLK(CLK_i),
            .FP_A(A_i),
            .FP_B(B_i),
            .FP_Z(Z_i));
   

   data_sink DS(.RST_n(RST_n_i),
		         .CLK(CLK_i),
		         .VIN(VOUT_i),
		         .Z_IN(Z_i));   

endmodule
